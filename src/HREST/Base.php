<?php
namespace HREST;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 01.10.2015
 * Time: 19:31
 */
abstract class Base extends \Core_Servant_Properties
{
	const LOGIN_NOT_FOUND_CODE = 100;
	const INVALID_PASSWORD_CODE = 101;
	const INVALID_TOKEN_CODE = 102;
	const FACEBOOK_LOGIN_ERROR_CODE = 103;
	const USER_NOT_FOUND_CODE = 104;

	const IOT_GATEWAY_DISABLE_CODE = 200;
	const IOT_GATEWAY_NOT_AVAILABLE_CODE = 201;
	const PUSH_NOTIFICATION_SERVICE_INVALID_CODE = 202;

	const SIGNATURE_INVALID_CODE = 300;
	const NO_PERMISSION_CODE = 301;
	const INVALID_SECRET_CODE = 302;
	const ACCOUNT_BLOCKED_CODE = 303;

	const VALIDATION_ERROR_CODE = 400;
	const INVALID_JSON_FORMAT_CODE = 401;
	const RESOURCE_NOT_FOUND_CODE = 402;
	const REGISTRATION_LOGIN_EXISTS_CODE = 403;
	const REGISTRATION_INVALID_CODE = 404;
	const REGISTRATION_EXPIRED_CODE = 405;
	const INVALID_PARAMS = 406;
	const TOO_MANY_ACCOUNT_CODE = 407;
	const UNIQUE_NAME_CONFLICT_CODE = 408;
	const EMPTY_BODY_CODE = 409;
	const UNDEFINED_REQUEST_CODE = 411;

	protected $_siteuser = array();
	protected $_su_groups = array();
	protected $_options = array();
	/**
	 * Valid object properties
	 * @var array
	 */
	protected $_validProperties = array();

	protected $_instance = array();

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions($options)
	{
		$this->_options = $options;
		return $this;
	}

	/**
	 * Connect constructor.
	 */
	public function __construct($options=array())
	{
		parent::__construct();

		$this->_instance = \Core_Page::instance();
		/** @var \Siteuser_Model $oSu */
		$this->_siteuser = \Core_Entity::factory('Siteuser')->getCurrent();
		/** @var \Siteuser_Group_Model $oGroups */
		$oGroups = \Core_Entity::factory('Siteuser_Group');
		if(!(isset($this->_siteuser->id) && $this->_siteuser->id > 0)) {
			$this->_siteuser = false;
		} else {
			$this->_su_groups = $oGroups->getForSiteuser($this->_siteuser->id);
		}
		if(!(is_array($this->_su_groups)
			&& count($this->_su_groups)
			&& isset($this->_su_groups[0]->id)
			&& $this->_su_groups[0]->id > 0
			)
		) {
			$this->_su_groups = false;
		}
		foreach($_GET as $getRequestKey => $getRequestValue) {
			$propertyName = 'get'.ucfirst(strtolower(preg_replace('/\_/', '', $getRequestKey)));
			$this->addAllowedProperty($propertyName); //  _allowedProperties['get_'.$getRequestKey] = $getRequestValue;
			$this->$propertyName = $getRequestValue;
		}
		$postWorking = array();
		switch (\Core_Array::get($_SERVER, "CONTENT_TYPE", 'default')) {
			case 'application/json':
				$inputJSON = file_get_contents('php://input');
				$postWorking = json_decode($inputJSON, true);
				break;
			default:
				$postWorking = $_POST;
		}
		if(is_array($postWorking)) {
			foreach($postWorking as $postRequestKey => $postRequestValue) {
				$propertyName = 'post'.ucfirst(strtolower($postRequestKey));
				$this->addAllowedProperty($propertyName); //  _allowedProperties['get_'.$getRequestKey] = $getRequestValue;
				$this->$propertyName = $postRequestValue;
			}
		}
		$this->_options = $options;
	}

	public function getValue($property, $defaults)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			return $this->_propertiesValues[$property];
		}

		return $defaults;
	}

	public function getValues()
	{
		return $this->_propertiesValues;
	}

	/**
	 * Utilized for reading data from inaccessible properties
	 * @param string $property property name
	 * @return mixed
	 */
	public function __get($property)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			return $this->_propertiesValues[$property];
		}

		throw new \Core_Exception("The property '%property' does not exist in '%class'.",
			array('%property' => $property, '%class' => get_class($this)));
	}

	/**
	 * @param array $validProperties
	 */
	public function addValidProperty($validProperty)
	{
		$this->_validProperties[] = $validProperty;
	}

	/**
	 * @return array
	 */
	public function getValidProperties()
	{
		return $this->_validProperties;
	}

	/**
	 * @param array $validProperties
	 */
	public function setValidProperties($validProperties)
	{
		$this->_validProperties = $validProperties;
	}

	/**
	 * Run when writing data to inaccessible properties
	 * @param string $property property name
	 * @param string $value property value
	 * @return self
	 */
	public function __set($property, $value)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			$this->_propertiesValues[$property] = $value;
			return $this;
		}

		throw new \Core_Exception("The property '%property' does not exist in the entity",
			array('%property' => $property));
	}

	/**
	 * Check cache availability
	 * @return boolean
	 */
	public static function createInstance() {}

	public static function encodeUrl($str) {
		return str_replace('/', '\/', $str);
	}

	/**
	 */
	private function _validInputValues() {
		if(count($this->_validProperties)>0) {
			if(is_array($this->_validProperties) && count($this->_validProperties)>0) {
//				print_r($this->_propertiesValues);
//				print_r($this->_validProperties);
//				foreach ($this->_validProperties as $validPropertyKey => $validProperty) {
//					$this->_validateExpression($validPropertyKey, $validProperty);
//				}
				return true;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	private function _validateExpression($vpKey, $vpValues) {
		$retValue = false;

		foreach ($vpValues as $vpValueKey => $vpValue ) {
			if(is_array($vpValue)) {
				switch ($vpKey) {
					case 'AND':
						$retValue = $retValue & (isset($this->_propertiesValues[$vpValue]));
						break;
					case 'OR':
						$retValue = $retValue | (isset($this->_propertiesValues[$vpValue]));
						break;
				}
			} else {

			}
		}
//		if(isset($this->_propertiesValues[$vpValue])) {
//			$retValue = true;
//		}
		return $retValue;
	}

	/**
	 */
	public function execute() {
		return $this->_validInputValues();
	}
}