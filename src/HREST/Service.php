<?php
namespace HREST;

use Respect\Rest\Routable;
use Respect\Rest as rest;
use Core_Entity;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 25.09.2015
 * Time: 9:47
 */
class Service implements Routable
{
	/**
	 * @var array
	 */
	protected $_returns = array(
		'errDesc' => Base::IOT_GATEWAY_DISABLE_CODE." Ok.",
		'errCode' => Base::IOT_GATEWAY_DISABLE_CODE,
		'result' => "",
		'error' => "",
		'code' => Base::IOT_GATEWAY_DISABLE_CODE,
	);

	protected $_options = array(
		'stop_class' => array()
	);

	/**
	 * Service constructor.
	 * @param array $_returns
	 */
	public function __construct($options=array())
	{
		$this->_options = $options;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param $operation
	 * @return array
	 */
	public function get($operation, $method='')
	{
		$method==='' && $method=__FUNCTION__;
		$method=ucfirst($method);
		if(isset($operation[0])) {
			$sOperation = str_replace('_', '', ucfirst(strtolower(preg_replace('~.json~', '', $operation[0]))));
			$sOperationList = [];
			if(($operationsCnt=count($operation))>1) {
				for ($operationKey=1; $operationKey<=$operationsCnt; $operationKey++) {
					if(isset($operation[$operationKey])) {
						$sTmp = mb_strtolower($operation[$operationKey]);
						$sOperation .= '_'.\Core_Str::ucfirst($sTmp);
						unset($operation[$operationKey]);
					}
					if(isset($this->_options['stop_class']) && in_array($sTmp, $this->_options['stop_class'])) {
						break;
					}
				}
				unset($operation[0]);
				$sOperationList = $operation;
			}
			$this->_options['stop'] = array_values($operation);
			$baseClass = $this->_options['namespaces'][0];
			if(class_exists($processingClass = "{$baseClass}\\{$sOperation}")) {
				$this->_returns = $processingClass::createInstance()
					->execute();
			} else {
				$exception = true;
				$contentType = \Core_Array::get($_SERVER, 'HTTP_ACCEPT', 'text/html');
				switch ($contentType) {
					case 'application/xml':
						$_GET['responsetype'] = 'XMLCONSOLE';
						break;
					case 'application/json':
						$_GET['responsetype'] = 'ARRAY';
						break;
					default:
						$_GET['responsetype'] = 'HTML';
				}
				if(($optionsCount=count($this->_options['namespaces']))>1) {
					for($i=1; $i<$optionsCount; $i++) {
						if(class_exists($processingClass = "{$this->_options['namespaces'][$i]}_{$method}_{$sOperation}")) {
							$processInstance = $processingClass::createInstance();
							$this->_returns = $processInstance
								->setOptions($this->_options)
								->execute();
							$exception=false;
							break;
						}
					}
				}

				$exception && $this->_returns = array(
					'errDesc' => sprintf("Класс `%s` не существует.", $processingClass),
					'errCode' => Base::VALIDATION_ERROR_CODE,
					'result' => "",
					'error' => Base::VALIDATION_ERROR_CODE . " Bad Request. Неизвестная операция.",
					'code' => Base::VALIDATION_ERROR_CODE,
				);
			}
		} else {
			$this->_returns = array(
				'errDesc' => Base::VALIDATION_ERROR_CODE." Bad Request. Операция не определена.",
				'errCode' => Base::VALIDATION_ERROR_CODE,
				'result' => "",
				'error' => Base::VALIDATION_ERROR_CODE." Bad Request. Неизвестная операция.",
				'code' => Base::VALIDATION_ERROR_CODE,
			);
		}
		return $this->_returns;
	}

	/**
	 * @param $operation
	 * @return array
	 */
	public function post($operation)
	{
		return $this->get($operation, __FUNCTION__);
	}
}