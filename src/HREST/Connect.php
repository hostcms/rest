<?php
namespace HREST;

use Respect\Rest\Router as restRouter;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 25.09.2015
 * Time: 9:47
 */
class Connect {

	protected $_partPrefix = NULL;
	protected $_authFormMessage = 'Please, take login and password';
	protected $_restBasicLogin = NULL;
	protected $_restBasicPassword = NULL;
	protected $_restHttpAuth = NULL;
	protected $_userClass = '';
	protected $_options = '';

	protected $_namespaces = array('\HREST\Auth');

	/**
	 * Connect constructor.
	 */
	public function __construct($partPrefix='/', $userClass='', $options=[])
	{
		$this->_partPrefix = $partPrefix;
		$this->_userClass = $userClass;
		$this->_options = $options;
	}

	public static function createInstance($partPrefix='/rest', $userClass='', $options=[]) {
		return new self($partPrefix, $userClass, $options);
	}

	/**
	 * @return array
	 */
	public function getNamespaces()
	{
		return $this->_namespaces;
	}

	/**
	 * @param array $namespaces
	 */
	public function setNamespaces($namespaces)
	{
		$this->_namespaces = $namespaces;
		return $this;
	}

	/**
	 * @param array $namespaces
	 */
	public function addNamespace($namespace)
	{
		$this->_namespaces[] = $namespace;
		return $this;
	}

	/**
	 * @param array $namespaces
	 */
	public function setOption($optionKey, $option)
	{
		$this->_options[$optionKey] = $option;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAuthFormMessage()
	{
		return $this->_authFormMessage;
	}

	/**
	 * @param string $authFormMessage
	 */
	public function setAuthFormMessage($authFormMessage)
	{
		$this->_authFormMessage = $authFormMessage;
		return $this;
	}

	/**
	 * @param null $restBasicLogin
	 * @param null $restBasicPassword
	 */
	public function setRestBasicLoginAndPassword($restBasicLogin, $restBasicPassword)
	{
		$this->_restBasicLogin = $restBasicLogin;
		$this->_restBasicPassword = $restBasicPassword;
		return $this;
	}

	/**
	 * @param null $httpAuth
	 */
	public function setRestHTTPAuth($httpAuth)
	{
		$this->_restHttpAuth = $httpAuth;
		return $this;
	}

	public function checkAuth($user, $pass) {
//		\Skynetcore_Utils::p($_SERVER);
//		\Skynetcore_Utils::p($pass, 'pass');
		if($user != '' && $pass != '') {
			/** @var \Siteuser_Model $oSiteuser */
			$oSiteuser = \Core_Entity::factory('Siteuser');
			$oSiteuser->unsetCurrent();
			switch (true) {
				case in_array(\Core_Array::get($_SERVER, 'HTTP_AUTHORIZATION', 'none'), $this->_restHttpAuth):
					return true;
				case $user === $this->_restBasicLogin && $pass === $this->_restBasicPassword:
					return true;
				default:
					$siteUser = $oSiteuser->getByLoginAndPassword($user, $pass);
					if(isset($siteUser->id) && $siteUser->id > 0) {
						$siteUser->setCurrent();
						return true;
					}
			}
		}
		return false;
	}

	public function execute()
	{
		if(\Core_Array::get($_SERVER, 'HTTP_ACCEPT', false) === false) {
			$_SERVER['HTTP_ACCEPT'] = 'application/json';
		}
		$r3 = new restRouter();
		$this->_options = array(array_merge($this->_options,
			array('namespaces'=>$this->_namespaces)
		));
//		if(trim($this->_restBasicLogin)!='' && trim($this->_restBasicPassword)!='') {
//			$r3
//				->authBasic($this->_authFormMessage, function($user, $pass) {
//					return $user === $this->_restBasicLogin && $pass === $this->_restBasicPassword;
//				});
//		}
		$r3->errorRoute(function (array $err) {
			return 'Sorry, this errors happened: '.var_dump($err);
		});
		$r3->get($this->_partPrefix.'/**', 'HREST\Service', $this->_options)
			->authBasic($this->_authFormMessage, [$this, "checkAuth"])
			->accept(array(
//					'application/xml' => function($input) {
//						return $input['entity'];
//					},
//					'text/html' => function($data) {
//						list($k,$v)=each($data);
//						return "$k: $v";
//					},
					'application/json' => function($input) {
						header('Content-type: application/json; charset=utf-8');
						if(isset($input['code']) && $input['code']!='') {
							header("HTTP/1.1 {$input['code']}");
						}
						return json_encode($input, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
					},
				)
			)
		;
		$r3->post($this->_partPrefix.'/**', 'HREST\Service', $this->_options)
			->authBasic($this->_authFormMessage, [$this, "checkAuth"])
			->accept(array(
					'application/json' => function($input) {
						if(isset($input['code']) && $input['code']!='') {
							header("HTTP/1.1 {$input['code']}");
						}
						return json_encode($input, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
					},
				)
			)
		;
	}
}